import os
from fastapi import FastAPI, HTTPException, Request
from fastapi.responses import StreamingResponse
from pydantic_settings import BaseSettings
from typing import Union
from .iso import Iso
from .models.iso_builder import IsoBuilder

summary = """
    API which will create and manage cloudinit ISO files
"""

description = """
    API which will allow creating and downloading cloudinit ISO files
    It will run as a proxmox companion to get cloudinit working without
    needing a ssh connection to proxmox hosts
"""


class Settings(BaseSettings):
    iso_folder: str = "../iso"


settings = Settings()
app = FastAPI(
    title="Cloud-init Iso Builder",
    summary=summary,
    description=description,
)


@app.get("/iso/{id}")
def download_iso(id: str):
    iso_path = os.path.join(os.path.abspath(settings.iso_folder), f"{id}.iso")
    if not os.path.exists(iso_path):
        raise HTTPException(status_code=404, detail="Iso not found")

    def iterfile():
        with open(iso_path, mode="rb") as file:
            yield from file

    return StreamingResponse(iterfile(), media_type="application/octet-stream")


@app.post("/iso")
def create_iso(content: IsoBuilder, request: Request):
    iso_builder = Iso(work_folder=settings.iso_folder)
    iso_builder.create(content=content.files)
    return {
        "result": "OK",
        "url": request.url_for("download_iso", id=iso_builder.iso_id),
    }


@app.delete("/iso/{id}")
def delete_iso(id: str):
    return {"action": f"deleting {id}"}
