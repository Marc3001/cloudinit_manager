from io import BytesIO
import os
import random
import re
import string
import pycdlib


class Iso:
    def __init__(self, work_folder):
        self.work_folder = os.path.abspath(work_folder)

    @property
    def filename(self):
        return f"{self.iso_id}.iso"

    def create(self, content={}) -> None:
        """
        Will create iso file from files and their content provided
        """
        self.iso_id = "".join(random.choices(string.ascii_lowercase, k=16))
        iso = pycdlib.PyCdlib()
        iso.new(rock_ridge="1.09", joliet=3)
        for file in content:
            iso.add_fp(
                BytesIO(bytes(content[file], "utf8")),
                len(content[file]),
                f"/{''.join(random.choices(string.ascii_uppercase, k=6))}.;1",
                rr_name=file,
                joliet_path=f"/{file}",
            )
        iso.write(os.path.join(self.work_folder, self.filename))
        iso.close()
