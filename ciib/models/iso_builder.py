from functools import cached_property
from pydantic import BaseModel, Field, computed_field


class IsoBuilder(BaseModel):
    metadata: str = Field(alias="meta-data")
    userdata: str = Field(alias="user-data")
    vendordata: str | None = Field(default=None, alias="vendor-data")
    networkconfig: str | None = Field(default=None, alias="network-config")

    @computed_field
    @cached_property
    def files(self) -> list:
        """
        Will return list of files and their content as a dict
        filename will be attributed from field alias or filed name
        """
        result = {}
        for field in self.model_fields:
            value = getattr(self, field)
            alias = self.model_fields[field].alias
            if value is not None:
                filename = alias if alias is not None else field
                result[filename] = value
        return result
